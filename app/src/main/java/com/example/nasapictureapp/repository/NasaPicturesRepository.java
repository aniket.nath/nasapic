package com.example.nasapictureapp.repository;

import com.example.nasapictureapp.model.PictureDetails;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

@Singleton
public class NasaPicturesRepository {

    private MutableLiveData<List<PictureDetails>> mPictureDetailsListMutableLiveData
            = new MutableLiveData<>();

    @Inject
    public NasaPicturesRepository(List<PictureDetails> pictureDetailsList) {
        mPictureDetailsListMutableLiveData.setValue(pictureDetailsList);
    }

    public LiveData<List<PictureDetails>> getPictureDetailsList() {
        return mPictureDetailsListMutableLiveData;
    }
}
