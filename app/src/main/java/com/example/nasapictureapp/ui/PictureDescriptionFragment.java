package com.example.nasapictureapp.ui;

import com.example.nasapictureapp.R;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class PictureDescriptionFragment extends DialogFragment {

    public static final String DESCRIPTION_TEXT_KEY = "description";
    private String mPictureDescription;
    private View mRoot;

    public PictureDescriptionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mRoot = inflater.inflate(R.layout.fragment_picture_description, container, false);
        initArguments(getArguments());
        setUpDescriptionTextView();
        return mRoot;
    }

    private void setUpDescriptionTextView() {
        TextView pictureDescriptionTextView = mRoot.findViewById(R.id.description_text_view);
        pictureDescriptionTextView.setText(mPictureDescription);
    }

    private void initArguments(Bundle arguments) {
        if (arguments != null) {
            mPictureDescription = arguments.getString(DESCRIPTION_TEXT_KEY);
        }
    }

}
