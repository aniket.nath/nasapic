package com.example.nasapictureapp.ui;

import com.example.nasapictureapp.R;
import com.example.nasapictureapp.adapter.NasaSlideShowImageAdapter;
import com.example.nasapictureapp.model.PictureDetails;

import android.os.Bundle;

import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

public class NasaImageShowActivity extends AppCompatActivity {

    public static String CLICKED_IMAGE_POSITION_INTENT_EXTRA_CONSTANT = "position";
    public static String PICTURE_DETAILS_LIST_INTENT_EXTRA_CONSTANT = "picture_details_list";
    private List<PictureDetails> mPictureDetailsList;
    private int mPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nasa_image_show);
        initArguments();
        setUpViewPager();
    }

    private void initArguments() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mPosition = extras.getInt(CLICKED_IMAGE_POSITION_INTENT_EXTRA_CONSTANT,
                    0);
            mPictureDetailsList =
                    extras.getParcelableArrayList(
                            PICTURE_DETAILS_LIST_INTENT_EXTRA_CONSTANT);
        }
    }

    private void setUpViewPager() {
        ViewPager nasaImageSlideShowViewPager =
                findViewById(R.id.nasa_image_slide_show_viewpager);
        NasaSlideShowImageAdapter nasaSlideShowImageAdapter =
                new NasaSlideShowImageAdapter(this,
                mPictureDetailsList);
        nasaImageSlideShowViewPager.setAdapter(nasaSlideShowImageAdapter);
        nasaImageSlideShowViewPager.setCurrentItem(mPosition);
    }
}
