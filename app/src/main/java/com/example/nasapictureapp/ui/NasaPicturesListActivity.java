package com.example.nasapictureapp.ui;

import com.example.nasapictureapp.R;
import com.example.nasapictureapp.adapter.NasaPicturesAdapter;
import com.example.nasapictureapp.viewmodel.NasaPicturesViewModel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dagger.android.AndroidInjection;

import android.os.Bundle;

import javax.inject.Inject;

public class NasaPicturesListActivity extends AppCompatActivity {

    private NasaPicturesAdapter mNasaPicturesAdapter;
    @Inject
    ViewModelProvider.Factory mNasaPictureListViewModelFactory;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nasa_picture_list);
        injectActivity();
        setUpRecyclerView();
        setUpViewModel();
    }

    private void injectActivity() {
        AndroidInjection.inject(this);
    }

    private void setUpRecyclerView() {
        RecyclerView nasaPicturesList = findViewById(R.id.nasa_pictures_list);
        mNasaPicturesAdapter = new NasaPicturesAdapter(this);
        nasaPicturesList.setLayoutManager(new GridLayoutManager(this, 2));
        nasaPicturesList.setAdapter(mNasaPicturesAdapter);
    }

    private void setUpViewModel() {
        NasaPicturesViewModel viewModel = ViewModelProviders
                .of(this, mNasaPictureListViewModelFactory)
                .get(NasaPicturesViewModel.class);
        viewModel.getPictureDetailsList().observe(this, responses -> {
            if (responses != null && responses.size() > 0) {
                mNasaPicturesAdapter.setNasaPicturesList(responses);
            }
        });
    }
}
