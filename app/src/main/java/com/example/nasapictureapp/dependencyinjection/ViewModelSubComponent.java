package com.example.nasapictureapp.dependencyinjection;

import com.example.nasapictureapp.viewmodel.NasaPicturesViewModel;

import dagger.Subcomponent;

@Subcomponent
public interface ViewModelSubComponent {

    @Subcomponent.Builder
    interface Builder {
        ViewModelSubComponent build();
    }

    NasaPicturesViewModel nasaPictureViewModel();
}
