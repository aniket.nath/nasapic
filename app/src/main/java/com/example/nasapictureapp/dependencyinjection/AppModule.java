package com.example.nasapictureapp.dependencyinjection;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import com.example.nasapictureapp.R;
import com.example.nasapictureapp.model.PictureDetails;
import com.example.nasapictureapp.repository.NasaPicturesRepository;
import com.example.nasapictureapp.viewmodel.NasaPicturesViewModelFactory;

import android.app.Application;
import android.util.Log;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import androidx.lifecycle.ViewModelProvider;
import dagger.Module;
import dagger.Provides;

@Module(subcomponents = ViewModelSubComponent.class)
class AppModule {
    private static final Type PICTURE_DETAILS_FORMAT = new TypeToken<List<PictureDetails>>() {
    }.getType();

    @Singleton
    @Provides
    List<PictureDetails> getPictureDetailsList(
            Application nasaPictureListApplication) {
        Gson gson = new Gson();
        List<PictureDetails> pictureDetails = new ArrayList<PictureDetails>();
        try {
            String pictureDetailsFileName = "data.json";
            InputStream stream = nasaPictureListApplication.getAssets()
                    .open(pictureDetailsFileName);
            JsonReader reader = new JsonReader(new InputStreamReader(stream));
            pictureDetails = gson.fromJson(reader, PICTURE_DETAILS_FORMAT);
        } catch (FileNotFoundException e) {
            Log.d(nasaPictureListApplication.getString(R.string.ERROR_IN_FILE_REPORT),
                    e.toString());
        } catch (IOException a) {
            Log.d(nasaPictureListApplication.getString(R.string.ERROR_IN_IO_REPORT),
                    a.toString());
        }
        return pictureDetails;
    }

    @Singleton
    @Provides
    NasaPicturesRepository provideRepository(List<PictureDetails> pictureDetails) {
        return new NasaPicturesRepository(pictureDetails);
    }

    @Singleton
    @Provides
    ViewModelProvider.Factory provideViewModelFactory(
            ViewModelSubComponent.Builder viewModelSubComponent) {
        return new NasaPicturesViewModelFactory(viewModelSubComponent.build());
    }
}
