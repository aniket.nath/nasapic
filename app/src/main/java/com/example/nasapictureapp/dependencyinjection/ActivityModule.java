package com.example.nasapictureapp.dependencyinjection;

import com.example.nasapictureapp.ui.NasaPicturesListActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract NasaPicturesListActivity contributeNasaPicturesListActivity();
}
