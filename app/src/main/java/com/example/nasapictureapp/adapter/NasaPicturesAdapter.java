package com.example.nasapictureapp.adapter;

import com.example.nasapictureapp.R;
import com.example.nasapictureapp.model.PictureDetails;
import com.example.nasapictureapp.ui.NasaImageShowActivity;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NasaPicturesAdapter
        extends RecyclerView.Adapter<NasaPicturesAdapter.NasaPicturesViewHolder> {

    class OnImageClickListener implements View.OnClickListener {
        int mPosition;
        OnImageClickListener(int position) {
            mPosition = position;
        }
        @Override
        public void onClick(View v) {
            Intent startNasaImageShowActivity = new Intent(mContext, NasaImageShowActivity.class);
            startNasaImageShowActivity
                    .putExtra(NasaImageShowActivity.CLICKED_IMAGE_POSITION_INTENT_EXTRA_CONSTANT,
                            mPosition);
            startNasaImageShowActivity.putParcelableArrayListExtra(
                    NasaImageShowActivity.PICTURE_DETAILS_LIST_INTENT_EXTRA_CONSTANT,
                    (ArrayList<? extends Parcelable>) mNasaPicturesList);
            mContext.startActivity(startNasaImageShowActivity);
        }
    }

    private Context mContext;
    private List<PictureDetails> mNasaPicturesList;

    public NasaPicturesAdapter(Context context) {
        mContext = context;
    }

    @NonNull
    @Override
    public NasaPicturesViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.item_nasa_picture, viewGroup, false);
        return new NasaPicturesAdapter.NasaPicturesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NasaPicturesViewHolder nasaPicturesViewHolder,
            int position) {
        PictureDetails pictureDetails = mNasaPicturesList.get(position);
        Picasso.get().load(pictureDetails.getUrl()).placeholder(R.drawable.placeholder).fit()
                .into(nasaPicturesViewHolder.mNasaPictureItem);
        nasaPicturesViewHolder.mNasaPictureItem
                .setOnClickListener(new OnImageClickListener(position));
    }

    @Override
    public int getItemCount() {
        if (mNasaPicturesList == null) {
            return 0;
        }
        return mNasaPicturesList.size();
    }

    public void setNasaPicturesList(List<PictureDetails> nasaPicturesList) {
        mNasaPicturesList = nasaPicturesList;
        notifyDataSetChanged();
    }

    class NasaPicturesViewHolder extends RecyclerView.ViewHolder {
        private ImageView mNasaPictureItem;

        NasaPicturesViewHolder(@NonNull View itemView) {
            super(itemView);
            mNasaPictureItem = itemView.findViewById(R.id.nasa_picture);
        }
    }
}
