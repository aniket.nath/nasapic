package com.example.nasapictureapp.adapter;

import com.example.nasapictureapp.R;
import com.example.nasapictureapp.model.PictureDetails;
import com.example.nasapictureapp.ui.PictureDescriptionFragment;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;

public class NasaSlideShowImageAdapter extends PagerAdapter {
    private List<PictureDetails> mPictureDetailsList;
    private PictureDetails mPictureDetails;
    private Context mContext;

    public NasaSlideShowImageAdapter(Context context, List<PictureDetails> pictureDetailsList) {
        mContext = context;
        mPictureDetailsList = pictureDetailsList;
    }

    @Override
    public int getCount() {
        return mPictureDetailsList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater
                .inflate(R.layout.viewpager_item_show_image_full_screen_layout, container,
                        false);
        ImageView displayFullScreenImageView = viewLayout
                .findViewById(R.id.image_view_full_screen);
        TextView pictureTitleTextView = viewLayout
                .findViewById(R.id.picture_title_text_view);
        TextView pictureDescriptionTextView = viewLayout
                .findViewById(R.id.picture_description_text_view);
        mPictureDetails = mPictureDetailsList.get(position);
        Picasso.get().load(mPictureDetails.getUrl())
                .placeholder(R.drawable.placeholder).fit()
                .into(displayFullScreenImageView);
        pictureTitleTextView.setText(mPictureDetails.getTitle());
        pictureDescriptionTextView.setOnClickListener(new OnDescriptionClickListener());
        container.addView(viewLayout);
        return viewLayout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container,
            int position,
            @NonNull Object object) {
        container.removeView((View) object);
    }

    class OnDescriptionClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            PictureDescriptionFragment pictureDescriptionFragment
                    = new PictureDescriptionFragment();
            Bundle arguments = new Bundle();
            arguments.putString(PictureDescriptionFragment.DESCRIPTION_TEXT_KEY,
                    mPictureDetails.getExplanation());
            pictureDescriptionFragment.setArguments(arguments);
            pictureDescriptionFragment
                    .show(((AppCompatActivity) mContext).getSupportFragmentManager(),
                            pictureDescriptionFragment.getTag());
        }
    }
}
