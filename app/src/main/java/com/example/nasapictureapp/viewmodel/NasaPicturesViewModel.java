package com.example.nasapictureapp.viewmodel;

import com.example.nasapictureapp.model.PictureDetails;
import com.example.nasapictureapp.repository.NasaPicturesRepository;

import android.app.Application;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class NasaPicturesViewModel extends AndroidViewModel {

    private LiveData<List<PictureDetails>> mPictureDetailsListLiveData = null;
    private NasaPicturesRepository mNasaPicturesRepository;

    @Inject
    public NasaPicturesViewModel(NasaPicturesRepository nasaPicturesRepository,
            @NonNull Application application) {
        super(application);
        mNasaPicturesRepository = nasaPicturesRepository;
    }

    public LiveData<List<PictureDetails>> getPictureDetailsList() {
        if (mPictureDetailsListLiveData == null) {
            mPictureDetailsListLiveData = mNasaPicturesRepository.getPictureDetailsList();
        }
        return mPictureDetailsListLiveData;
    }
}
