package com.example.nasapictureapp.viewmodel;

import com.example.nasapictureapp.dependencyinjection.ViewModelSubComponent;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class NasaPicturesViewModelFactory implements ViewModelProvider.Factory {

    private Map<Class, Callable<? extends ViewModel>> creators;

    @Inject
    public NasaPicturesViewModelFactory(ViewModelSubComponent viewModelSubComponent) {
        creators = new HashMap<>();
        creators.put(NasaPicturesViewModel.class, viewModelSubComponent::nasaPictureViewModel);
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        Callable<? extends ViewModel> creator = creators.get(modelClass);
        if (creator == null) {
            for (Map.Entry<Class, Callable<? extends ViewModel>> entry : creators.entrySet()) {
                if (modelClass.isAssignableFrom(entry.getKey())) {
                    creator = entry.getValue();
                    break;
                }
            }
        }
        if (creator == null) {
            throw new IllegalArgumentException("unknown model class " + modelClass);
        }
        try {
            final T call = (T) creator.call();
            return call;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
}
