**NasaPic Project : **

Screens : 

There are two screens in the project

1. On the first screen ypu can see list of Nasa pictures in tabular form and every row contains two images
2. If you click on any image it will navigate on the second screen
3. On the second screen you can see the full image and you can swap next or privious images
4. On the top of the image you can see the title of the image
5. At the bottom of the image you can find a text as "Read Description...."
6. By clicking on the page the full description of the image will be visible on a pop-up.

Code structure :

Code is written in MVVM(Model-View-ViewModel) format

Libraries Used : 

Gson

Picasso

Android Lifecycle Libraries

Dagger2


